#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>

void error_handling(char *message);

int main(int argc, char* argv[]) 
{
	int sock;
	struct sockaddr_in serv_addr;
	char message[30];
	int str_len;
	
	if(argc != 3)
	{
		printf("Usage : %s <IP> <port>\n", argv[0]);
		exit(1);
	}

	sock = socket(PF_INET, SOCK_STREAM, 0); // 소켓 생성 (프로토콜 체계 PF_INET = IPv4 인터넷 프로토콜 , TCP 통신) 참고(https://ehpub.co.kr/tag/sock_stream/)
	if(sock == -1)
		error_handling("socket() error");
	memset(&serv_addr, 0, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET; // 주소 체계 IPv4 인터넷 프로토콜
	serv_addr.sin_addr.s_addr = inet_addr(argv[1]); // ip 주소
	serv_addr.sin_port = htons(atoi(argv[2])); // 포트
	
	if(connect(sock, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) == -1) // 연결요청
		error_handling("connect() errror!");
	
	str_len = read(sock, message, sizeof(message)-1); // read로 읽어들이기
	if(str_len == -1)
		error_handling("read() error!");
	
	printf("Message from server : %s \n", message);
	close(sock);
	return 0;
}

void error_handling(char *message)
{
	fputs(message, stderr);
	fputc('\n', stderr);
	exit(1);
}